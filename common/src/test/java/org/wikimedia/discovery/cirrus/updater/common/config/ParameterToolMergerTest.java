package org.wikimedia.discovery.cirrus.updater.common.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import org.apache.flink.api.java.utils.ParameterTool;
import org.junit.jupiter.api.Test;

class ParameterToolMergerTest {

    private static final String DEFAULTS_LOCATION =
            "/org/wikimedia/discovery/cirrus/updater/common/config/ParameterToolMergerTest/defaults.properties";

    @Test
    void with_nothing() throws IOException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(null, new String[] {});
        assertThat(parameterTool.toMap()).isEmpty();
    }

    @Test
    void with_defaults() throws IOException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(DEFAULTS_LOCATION, new String[] {});
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("42");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("default");
    }

    @Test
    void with_defaults_and_file_overrides() throws IOException, URISyntaxException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        DEFAULTS_LOCATION,
                        new String[] {
                            Paths.get(
                                            getClass()
                                                    .getResource(getClass().getSimpleName() + "/overrides.properties")
                                                    .toURI())
                                    .toAbsolutePath()
                                    .toString()
                        });
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("84");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("default");
    }

    @Test
    void with_defaults_and_arg_overrides() throws IOException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        DEFAULTS_LOCATION, new String[] {"--c", "overridden"});
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("42");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("overridden");
    }

    @Test
    void with_defaults_and_file_overrides_and_arg_overrides() throws IOException, URISyntaxException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        DEFAULTS_LOCATION,
                        new String[] {
                            Paths.get(
                                            getClass()
                                                    .getResource(getClass().getSimpleName() + "/overrides.properties")
                                                    .toURI())
                                    .toAbsolutePath()
                                    .toString(),
                            "--c",
                            "overridden"
                        });
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("84");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("overridden");
    }

    @Test
    void fails_if_defaults_file_do_not_exist() {
        assertThatThrownBy(
                        () -> {
                            ParameterToolMerger.fromDefaultsWithOverrides("/missing.properties", new String[] {});
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void fails_if_overrides_file_do_not_exist() {
        assertThatThrownBy(
                        () -> {
                            ParameterToolMerger.fromDefaultsWithOverrides(
                                    null, new String[] {"/missing.properties"});
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }
}
