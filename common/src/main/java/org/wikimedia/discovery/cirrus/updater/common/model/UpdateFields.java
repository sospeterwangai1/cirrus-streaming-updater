package org.wikimedia.discovery.cirrus.updater.common.model;

import java.util.Map;

import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.types.Row;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowSerializationSchema;

public final class UpdateFields {

    public static final String OPERATION_UPDATE_REVISION = "update_revision";
    public static final String OPERATION_DELETE = "delete";

    public static final String PAGE_ID = "page_id";
    public static final String REV_ID = "rev_id";
    public static final String FIELDS = "fields";
    public static final String OPERATION = "operation";
    public static final String PAGE_NAMESPACE = "page_namespace";
    public static final String NOOP_HINTS = "noop_hints";
    public static final String INDEX_NAME = "index_name";

    private UpdateFields() {}

    public static Row getFields(Row updateEvent) {
        return updateEvent.getFieldAs(FIELDS);
    }

    public static String getIndexName(Row updateEvent) {
        return updateEvent.getFieldAs(INDEX_NAME);
    }

    public static Map<String, String> getNoopHints(Row updateEvent) {
        return updateEvent.getFieldAs(NOOP_HINTS);
    }

    public static String getOperation(Row updateEvent) {
        return updateEvent.getFieldAs(OPERATION);
    }

    public static Long getPageId(Row row) {
        return row.getFieldAs(PAGE_ID);
    }

    public static Long getRevId(Row row) {
        return row.getFieldAs(REV_ID);
    }

    public static JsonRowSerializationSchema buildFieldsRowSerializer(RowTypeInfo updateTypeInfo) {
        return JsonRowSerializationSchema.builder()
                .withTypeInfo(updateTypeInfo.getTypeAt(UpdateFields.FIELDS))
                .withoutNormalization()
                .build();
    }
}
