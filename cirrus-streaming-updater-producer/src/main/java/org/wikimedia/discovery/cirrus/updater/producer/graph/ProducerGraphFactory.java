package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.time.Duration;
import java.util.stream.IntStream;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.api.java.typeutils.TypeExtractor;
import org.apache.flink.connector.kafka.sink.KafkaSinkBuilder;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.AsyncDataStream.OutputMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.operators.async.AsyncWaitOperatorFactory;
import org.apache.flink.types.Row;
import org.apache.flink.util.OutputTag;
import org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.sink.UpdateRowEncoder;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.KafkaRecordTimestampStrategy;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
public class ProducerGraphFactory {

    public static final String UPDATER_OUTPUT_SCHEMA_VERSION = "1.0.0";
    public static final String UPDATER_OUTPUT_STREAM_NAME = "cirrussearch.update_pipeline.update";
    public static final String FETCH_FAILURE_STREAM = "cirrussearch.update_pipeline.fetch_failure";
    public static final String FETCH_FAILURE_SCHEMA_VERSION = "1.0.0";

    private final ProducerConfig config;

    private final EventDataStreamFactory eventDataStreamFactory;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public ProducerGraphFactory(ProducerConfig config) {
        this.config = config;
        this.eventDataStreamFactory =
                EventDataStreamFactory.from(
                        config.eventStreamJsonSchemaUrls(), config.eventStreamConfigUrl());
    }

    public static boolean filterInputEvents(InputEvent inputEvent) {
        return inputEvent.getDomain() != null && !inputEvent.getDomain().equals("canary");
    }

    public DataStream<InputEvent> createDataStreamSource(StreamExecutionEnvironment env) {
        KafkaSourceBuilder<Row> sourceBuilder =
                eventDataStreamFactory.kafkaSourceBuilder(
                        config.pageChangeStream(),
                        config.kafkaSourceBootstrapServers(),
                        config.kafkaSourceGroupId());
        setKafkaOffsetBounds(sourceBuilder);
        sourceBuilder.setProperties(config.kafkaSourceProperties());
        KafkaSource<Row> source = sourceBuilder.build();

        WatermarkStrategy<Row> wmStrategy =
                WatermarkStrategy.<Row>forBoundedOutOfOrderness(Duration.ofSeconds(30))
                        // set idleness of the source, usefull to not block the stream if some sources might
                        // become idle
                        .withIdleness(Duration.ofSeconds(30));
        return env.fromSource(source, wmStrategy, config.pageChangeStream() + "-source")
                .map(InputEventConverters::fromSource)
                .map(new IngestionTimeAssigner(config.clock()));
    }

    public Sink<Row> createDataSink() {
        KafkaSinkBuilder<Row> sinkBuilder =
                eventDataStreamFactory.kafkaSinkBuilder(
                        UPDATER_OUTPUT_STREAM_NAME,
                        UPDATER_OUTPUT_SCHEMA_VERSION,
                        config.kafkaSinkBootstrapServers(),
                        config.fetchSuccessTopic(),
                        KafkaRecordTimestampStrategy.FLINK_RECORD_EVENT_TIME);
        sinkBuilder.setKafkaProducerConfig(config.kafkaSinkProperties());
        return sinkBuilder.build();
    }

    public EventRowTypeInfo createOutputTypeInfo() {
        return eventDataStreamFactory.rowTypeInfo(
                UPDATER_OUTPUT_STREAM_NAME, UPDATER_OUTPUT_SCHEMA_VERSION);
    }

    public TransformOperator<InputEvent, RetryContext<InputEvent, Row>> createFetchRevisionOperator(
            UpdateRowEncoder encoder) {
        final RevisionFetcher revisionFetcher =
                new RevisionFetcher(
                        config.fetchRequestTimeout(),
                        new DefaultRevisionFetcherUriBuilder(),
                        encoder,
                        config.httpRoutes());

        final BypassingRevisionFetcher bypassingRevisionFetcher =
                new BypassingRevisionFetcher(revisionFetcher, encoder);

        final RevisionFetcherRetryPredicate revisionFetcherRetryPredicate =
                new RevisionFetcherRetryPredicate(
                        config.retryFetchMaxAge(), config.clock(), config.retryFetchMax());

        final RetryingAsyncFunction<
                        InputEvent, Row, BypassingRevisionFetcher, RevisionFetcherRetryPredicate>
                retryingFetchRevisionFunction =
                        new RetryingAsyncFunction<>(bypassingRevisionFetcher, revisionFetcherRetryPredicate);

        final long retryFetchFailTimeout =
                (long)
                        (Math.max(
                                        (config.retryFetchMaxAge().toMillis()
                                                + config.fetchRequestTimeout().toMillis()),
                                        IntStream.range(0, config.retryFetchMax())
                                                .mapToObj(retryingFetchRevisionFunction::calculateDelay)
                                                .mapToLong(Duration::toMillis)
                                                .map(delay -> delay + config.fetchRequestTimeout().toMillis())
                                                .sum())
                                * 1.2);

        final AsyncWaitOperatorFactory<InputEvent, RetryContext<InputEvent, Row>>
                fetchRevisionOperatorFactory =
                        new AsyncWaitOperatorFactory<>(
                                retryingFetchRevisionFunction,
                                retryFetchFailTimeout,
                                config.retryFetchQueueCapacity(),
                                OutputMode.UNORDERED);

        final TypeInformation<RetryContext<InputEvent, Row>> enrichedTypeInformation =
                RetryContextTypeInformation.create(
                        TypeExtractor.createTypeInfo(InputEvent.class), encoder.getOutputTypeInfo());

        return new TransformOperator<>(
                "enrich-page-change-with-revision", enrichedTypeInformation, fetchRevisionOperatorFactory);
    }

    public Sink<Row> createFetchFailureSink() {
        KafkaSinkBuilder<Row> sinkBuilder =
                eventDataStreamFactory.kafkaSinkBuilder(
                        FETCH_FAILURE_STREAM,
                        FETCH_FAILURE_SCHEMA_VERSION,
                        config.kafkaSinkBootstrapServers(),
                        config.fetchFailureTopic(),
                        KafkaRecordTimestampStrategy.FLINK_RECORD_EVENT_TIME);

        return sinkBuilder.build();
    }

    public MapFunction<Row, Row> createNamespaceIndexMapper() {
        return new CirrusNamespaceIndexMap(config.httpRoutes(), config.fetchRequestTimeout());
    }

    public ProcessFunction<RetryContext<InputEvent, Row>, Row> createFetchFailureRouter(
            OutputTag<Row> tag) {
        return new RouteFetchFailures(tag, config.pipeline());
    }

    public OutputTag<Row> createFetchFailureOutputTag() {
        return new OutputTag<>(
                "fetch_failures",
                eventDataStreamFactory.rowTypeInfo(FETCH_FAILURE_STREAM, FETCH_FAILURE_SCHEMA_VERSION));
    }

    public void createStreamGraph(StreamExecutionEnvironment env) {
        final Sink<Row> updateSink = createDataSink();
        final MapFunction<Row, Row> namespaceIndexMapper = createNamespaceIndexMapper();
        final UpdateRowEncoder updateRowEncoder =
                new UpdateRowEncoder(createOutputTypeInfo(), objectMapper);

        final DataStream<InputEvent> pageChangeEvents =
                createDataStreamSource(env).filter(ProducerGraphFactory::filterInputEvents);

        final TransformOperator<InputEvent, RetryContext<InputEvent, Row>>
                fetchRevisionTransformParameters = createFetchRevisionOperator(updateRowEncoder);

        final OutputTag<Row> fetchFailureOutputTag = createFetchFailureOutputTag();

        final SingleOutputStreamOperator<Row> processedRevisionCreate =
                pageChangeEvents
                        .transform(
                                fetchRevisionTransformParameters.operatorName,
                                fetchRevisionTransformParameters.outTypeInformation,
                                fetchRevisionTransformParameters.operatorFactory)
                        .process(createFetchFailureRouter(fetchFailureOutputTag));

        processedRevisionCreate.getSideOutput(fetchFailureOutputTag).sinkTo(createFetchFailureSink());

        processedRevisionCreate.map(namespaceIndexMapper).sinkTo(updateSink);
    }

    private void setKafkaOffsetBounds(KafkaSourceBuilder<Row> sourceBuilder) {
        if (config.kafkaSourceStartTime() != null) {
            sourceBuilder.setStartingOffsets(
                    OffsetsInitializer.timestamp(config.kafkaSourceStartTime().toEpochMilli()));
        }
        if (config.kafkaSourceEndTime() != null) {
            sourceBuilder.setBounded(
                    OffsetsInitializer.timestamp(config.kafkaSourceEndTime().toEpochMilli()));
        }
    }
}
