package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.async.CompletableFutureBackPorts;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.sink.UpdateRowEncoder;

/**
 * Wrapper around {@link RevisionFetcher} that bypasses fetching revisions under certain conditions.
 *
 * <p>Allows skipping {@link ChangeType#PAGE_DELETE}s while maintaining the order of events from
 * flink's perspective.
 */
public class BypassingRevisionFetcher
        implements Function<InputEvent, CompletionStage<Row>>, Serializable, Closeable {

    private static final long serialVersionUID = -2752702548762385275L;
    private final RevisionFetcher delegate;
    private final UpdateRowEncoder encoder;

    public BypassingRevisionFetcher(RevisionFetcher delegate, UpdateRowEncoder encoder) {
        this.delegate = delegate;
        this.encoder = encoder;
    }

    @Override
    public CompletionStage<Row> apply(InputEvent inputEvent) {
        return inputEvent.getChangeType() == ChangeType.PAGE_DELETE
                ? CompletableFutureBackPorts.completedFuture(encoder.encodeInputEvent(inputEvent))
                : delegate.apply(inputEvent);
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }
}
