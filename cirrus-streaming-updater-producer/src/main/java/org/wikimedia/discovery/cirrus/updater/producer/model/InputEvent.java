package org.wikimedia.discovery.cirrus.updater.producer.model;

import java.time.Instant;

import lombok.Data;

@Data
public class InputEvent {
    ChangeType changeType;
    String domain;
    String requestId;
    String eventStream;
    Instant eventTime;
    String wikiId;
    Long pageId;
    String pageTitle;
    Long pageNamespace;
    Long revId;
    Instant ingestionTime;
}
