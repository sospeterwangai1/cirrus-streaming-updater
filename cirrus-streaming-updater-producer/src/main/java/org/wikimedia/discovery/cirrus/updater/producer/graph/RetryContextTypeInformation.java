package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.collect.ImmutableList.of;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INT_TYPE_INFO;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.GenericTypeInfo;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;

public final class RetryContextTypeInformation {

    private RetryContextTypeInformation() {}

    public static <I, O> PojoTypeInfo<RetryContext<I, O>> create(
            TypeInformation<I> inputTypeInfo, TypeInformation<O> outputTypeInfo) {
        return new PojoTypeInfo<>(
                new TypeHint<RetryContext<I, O>>() {}.getTypeInfo().getTypeClass(),
                getFields(inputTypeInfo, outputTypeInfo));
    }

    private static <I, O> List<PojoField> getFields(
            TypeInformation<I> inputTypeInfo, TypeInformation<O> outputTypeInfo) {
        try {
            final Field input = RetryContext.class.getDeclaredField("input");
            final Field output = RetryContext.class.getDeclaredField("output");
            final Field failure = RetryContext.class.getDeclaredField("failure");
            final Field iteration = RetryContext.class.getDeclaredField("iteration");
            return of(
                    new PojoField(iteration, INT_TYPE_INFO),
                    new PojoField(input, inputTypeInfo),
                    new PojoField(output, outputTypeInfo),
                    new PojoField(failure, new GenericTypeInfo<>(Throwable.class)));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(
                    "Failed to build type information for " + RetryContext.class, e);
        }
    }
}
