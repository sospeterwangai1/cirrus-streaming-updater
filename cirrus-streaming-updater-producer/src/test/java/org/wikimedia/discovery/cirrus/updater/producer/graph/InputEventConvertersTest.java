package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.Instant;

import org.apache.commons.io.IOUtils;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

class InputEventConvertersTest {

    @Test
    void test_revision_create_conversion() throws IOException {
        EventDataStreamFactory factory =
                EventDataStreamFactory.from(
                        singletonList("https://schema.wikimedia.org/repositories/primary/jsonschema"),
                        this.getClass().getResource("/event-stream-config.json").toString());
        byte[] eventData =
                IOUtils.toByteArray(
                        getClass().getResourceAsStream(getClass().getSimpleName() + "/page_change-event.json"));
        Row event = factory.deserializer("rc0.mediawiki.page_change").deserialize(eventData);
        InputEvent inputEvent = InputEventConverters.fromSource(event);
        assertThat(inputEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(inputEvent.getEventTime()).isEqualTo(Instant.parse("2022-11-22T15:28:50Z"));
        assertThat(inputEvent.getEventStream()).isEqualTo("rc0.mediawiki.page_change");
        assertThat(inputEvent.getWikiId()).isEqualTo("testwiki");
        assertThat(inputEvent.getRequestId()).isEqualTo("6d7355de-d623-4dcc-9691-ad58ecafe5d1");
        assertThat(inputEvent.getPageNamespace()).isEqualTo(2);
        assertThat(inputEvent.getPageId()).isEqualTo(147498);
        assertThat(inputEvent.getRevId()).isEqualTo(551141);
        assertThat(inputEvent.getIngestionTime()).isNull();
    }
}
