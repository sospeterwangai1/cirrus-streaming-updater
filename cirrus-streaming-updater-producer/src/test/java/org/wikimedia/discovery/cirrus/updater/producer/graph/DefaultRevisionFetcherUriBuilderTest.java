package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

class DefaultRevisionFetcherUriBuilderTest {

    private final DefaultRevisionFetcherUriBuilder builder = new DefaultRevisionFetcherUriBuilder();

    @Test
    void build() {
        InputEvent event = new InputEvent();
        event.setChangeType(ChangeType.REV_BASED_UPDATE);
        event.setRevId(42L);
        event.setDomain("local.wikimedia");
        final URI uri = builder.build(event);
        assertThat(uri.getScheme()).isEqualTo("https");
        assertThat(uri.getHost()).isEqualTo("local.wikimedia");
        assertThat(uri.getQuery()).contains("revids=42");
    }
}
