#!/bin/sh -x

export LOG4J_LEVEL=INFO

exec /bin/java -classpath 'lib/*:usrlib/*' \
    org.wikimedia.discovery.cirrus.updater.consumer.ConsumerApplication \
    --event-stream-config-url file:$(pwd)/resources/event-stream-config.json \
    --event-stream-json-schema-urls file:$(pwd)/resources/schema_repo \
    --elasticsearch-urls https://localhost:9243 \
    --update-stream cirrussearch.update_pipeline.update \
    --kafka-source-config.bootstrap.servers PLAINTEXT://localhost:9092 \
    --kafka-source-config.group.id test \
    --dry-run true
